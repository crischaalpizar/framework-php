<meta charset="UTF-8">
<meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">

<!-- Styles -->
<link rel="stylesheet" href="<?php echo WWWROOT?>/css/bootstrap.min.css">
<link href="<?php echo WWWROOT?>/lib/font-awesome/css/font-awesome.css" rel="stylesheet">

<!-- Toastr style -->
<link href="<?php echo WWWROOT?>/css/plugins/toastr/toastr.min.css" rel="stylesheet">

<!-- Gritter -->
<link href="<?php echo WWWROOT?>/js/plugins/gritter/jquery.gritter.css" rel="stylesheet">

<link href="<?php echo WWWROOT?>/css/animate.css" rel="stylesheet">
<link href="<?php echo WWWROOT?>/css/style.css" rel="stylesheet">

<!-- Importar Page CSS -->
<link rel="stylesheet" href="<?php echo WWWROOT?>/css<?php echo FILE_PATH . str_replace('php', 'css', FILE_NAME)?>">
