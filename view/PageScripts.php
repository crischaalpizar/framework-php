<!-- Scripts -->
<script src="<?php echo WWWROOT?>/js/popper.min.js"></script>
<script src="<?php echo WWWROOT?>/js/jquery/jquery-3.1.1.min.js"></script>
<script src="<?php echo WWWROOT?>/js/vendor/metisMenu/jquery.metisMenu.js"></script>
<script src="<?php echo WWWROOT?>/js/vendor/slimscroll/jquery.slimscroll.js"></script>
<script src="<?php echo WWWROOT?>/js/inspinia/inspinia.js"></script>
<script src="<?php echo WWWROOT?>/js/bootstrap/bootstrap.js"></script>
<script src="<?php echo WWWROOT?>/js/Utils.js"></script>

<!-- Importar Page JS -->
<script src="<?php echo WWWROOT?>/js<?php echo FILE_PATH . str_replace('php', 'js', FILE_NAME)?>"></script>