<?php
define('PROJECT_NAME', '/ProyectoPHP');

define('DOMAIN', isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME']);
define('PROTOCOL', isset($_SERVER['HTTPS']) && ($_SERVER['HTTPS'] === 'on' || $_SERVER['HTTPS'] === 1)
    || isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https' ? 'https' : 'http');
define('PORT', $_SERVER['SERVER_PORT']);
define('SITE_PATH', preg_replace('/index.php$/i', '', $_SERVER['PHP_SELF']));
define('SITE', PROTOCOL . '://' . DOMAIN . SITE_PATH);
define('SITE_ROOT', PROTOCOL . '://' . DOMAIN . PROJECT_NAME);

define('WWWROOT', SITE_ROOT.'/public');
define('PAGE_STYLE', $_SERVER['DOCUMENT_ROOT']. PROJECT_NAME .'/view/PageStyles.php');
define('PAGE_SCRIPT', $_SERVER['DOCUMENT_ROOT']. PROJECT_NAME .'/view/PageScripts.php');

define('NAVBAR', $_SERVER['DOCUMENT_ROOT']. PROJECT_NAME .'/view/navbar.php');

define('FILE_NAME', basename(SITE));
define('FILE_PATH', str_replace(FILE_NAME, '', str_replace(SITE_ROOT, '', SITE)));