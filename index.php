<?php include dirname(__FILE__, 1).'/settings.php'?>

<!DOCTYPE html>
<html lang="en">

<head>
    <?php include PAGE_STYLE?>
    <title>Home</title>
</head>

<body class="pace-done">
    
    <?php include NAVBAR?>


    <?php include PAGE_SCRIPT?>
</body>

</html>